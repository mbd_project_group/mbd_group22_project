import os
import json
from collections import Counter
from typing import Tuple, Union

import numpy as np
import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objects as go

import networkx as nx
import networkx.algorithms.community as nx_comm
from numpy import ndarray

PATH_TO_DATA = os.path.join('..', 'gd')
PATH_TO_NODE_LIST = os.path.join(PATH_TO_DATA, 'new_node_list')
PATH_TO_IMG = os.path.join('images')
TREND_DIR = os.path.join(PATH_TO_IMG, 'trends')
PATH_TO_CACHE = os.path.join('.', 'cache')

METRIC_DIR = 'dense_metrics'
GRAPH_DIR = 'dense_graphs'

EDGE_FILE_DEFAULT = 'part-00000-34b94678-ceb6-4fc6-814b-cf15a57c2aac.c000'
NODE_FILE_DEFAULT = 'part-00000-fca25355-af8a-47b6-8984-4b28d7e3ac0e.c000'

NODE_LIST_P_0_95_DEFAULT = 'part-00000-96ea7586-abae-4561-836c-b86a686d274e.c000'
EDGE_LIST_DENSE_DEFAULT = 'part-00000-23b8606b-2f21-4e13-823c-d7af8adea4c1.c000'

path_to_nodes = lambda idx: os.path.join(PATH_TO_DATA, 'new_node_list', f'Interval={idx}',
                                         f'{NODE_LIST_P_0_95_DEFAULT}.csv')
path_to_edges = lambda idx: os.path.join(PATH_TO_DATA, 'edge_list_dense', f'Interval={idx}',
                                         f'{EDGE_LIST_DENSE_DEFAULT}.csv')


def read_graph_from_file(year_index: int) -> nx.classes.graph.Graph:
    """Reads a graph from a file and returns the graph object.

    Parameters
    ----------
    year_index : int
        The year index to read the graph from.

    Returns
    -------
    graph : networkx.classes.graph.Graph
        The graph object.
    """

    # check if this year index has a graph file in the cache folder
    # if not, read the graph from the file

    path_to_graph = os.path.join(PATH_TO_CACHE, GRAPH_DIR, f'graph_{year_index}.gpickle')
    if os.path.exists(path_to_graph):
        print(f'Loading graph from cache: {path_to_graph}')
        return nx.read_gpickle(path_to_graph)
    else:
        print(f'No cache found: creating graph for year {year_index}...')

        node_list_name = path_to_nodes(year_index - 1970)
        edge_list_name = path_to_edges(year_index - 1970)

        # our edge_lists have a header at the top for transferability between programs
        # we need to skip the first line for this reason
        with open(edge_list_name, 'r') as f:
            edges = f.read().splitlines()

        # read the graph from the file
        graph = nx.parse_edgelist(
            edges[1:], comments=None,
            delimiter=';', nodetype=str, data=(("weight", int),), create_using=nx.Graph
        )

        # the node attributes are stored in a separate file again for transferability
        # the first line is the header, so we need to skip that
        node_attributes = pd.read_csv(
            node_list_name, delimiter=';', header=0
        ).set_index('Id').to_dict('index')

        # add the attributes to the graph
        # the node id is the index of the dataframe and is the same as the node id in the edge list Source and Target
        # the dictionary has only one entry, Label, which is the maine genre the user reviewed in this year
        nx.set_node_attributes(graph, node_attributes)

        # check if all the nodes have the attribute 'Label' and 'Reviewed'
        problem = 0
        for node in graph.nodes:
            if not hasattr(graph.nodes[node], 'Label') or not hasattr(graph.nodes[node], 'Reviewed'):
                # print(f'Node {node} has no Label/Reviewed attribute')
                problem += 1
        if problem:
            print(f'Found {problem} nodes without Label/Reviewed attribute, trying manually...')
            # set the attributes manually from the dataframe
            for node in graph.nodes:
                graph.nodes[node]['Label'] = node_attributes[node]['Label']
                graph.nodes[node]['Reviewed'] = node_attributes[node]['Reviewed']
    return graph


def get_graph_communities(graph: nx.classes.graph.Graph) -> None:
    """Returns the communities of a graph.

    Parameters
    ----------
    graph : networkx.classes.graph.Graph
        The graph object.
    """

    # use the Louvain algorithm to find communities
    # then set the node_attribute community to the community id
    communities = list(nx_comm.louvain_communities(graph))
    community_dict = {
        node: i
        for i, community in enumerate(communities) for node in community
    }
    nx.set_node_attributes(graph, community_dict, 'community')


def get_assortativity_coefficient(graph: nx.classes.graph.Graph) -> float:
    # calculate the assortativity coefficient
    return nx.algorithms.assortativity.attribute_assortativity_coefficient(graph, 'Label')


def get_average_entropy(graph: nx.classes.graph.Graph) -> tuple[Union[ndarray,float], Union[ndarray,float]]:
    """Returns the average entropy of the communities in a graph.

    Parameters
    ----------
    graph : networkx.classes.graph.Graph
        The graph object.

    Returns
    -------
    avg_entropy : float
        The average entropy of the communities in the graph.
    """

    entropies = get_entropies(graph)

    # return the average entropy
    return np.mean(entropies), np.std(entropies)


def get_entropies(graph):
    # get the unique number of communities
    num_communities = len(set(nx.get_node_attributes(graph, 'community').values()))
    # calculate the entropy for each community
    entropies = []
    for i in range(num_communities):
        # get the number of nodes in each community
        community_nodes = [node for node in graph.nodes if graph.nodes[node]['community'] == i]
        community = graph.subgraph(community_nodes)

        # get the number of nodes in each genre
        genre_counts = Counter([graph.nodes[node]['Label'] for node in community.nodes])

        # calculate the entropy
        entropies.append(
            -sum([count / len(community.nodes) * np.log2(count / len(community.nodes)) for count in
                  genre_counts.values()])
        )
    return entropies


def calculate_community_id(graph, override=False):
    # check if the graph nodes have a community attribute
    # if not, calculate the communities
    if not override:
        for node in graph.nodes:
            if not hasattr(graph.nodes[node], 'community'):
                get_graph_communities(graph)
                break
    else:
        get_graph_communities(graph)


def plot_homogeneity_of_community(graph: nx.classes.graph.Graph, title: str) -> None:
    """Plots the homogeneity of the communities of a graph.

    Parameters
    ----------
    graph : networkx.classes.graph.Graph
        The graph object.

    title : str
        The title of the plot.
    """

    # get the communities
    # remove nodes which have Label nan or 'Books'
    communities = pd.Series([
        graph.nodes[node]['community'] for node in graph.nodes
    ])
    # get the labels for these communities
    labels = pd.Series([
        graph.nodes[node]['Label'] for node in graph.nodes
    ])

    # get the number of nodes in each community
    community_sizes = communities.value_counts()

    # plot the histogram
    community_sizes.plot.hist(
        bins=max(5, round(np.sqrt(community_sizes.max() + 1))),
        title='Distribution of Communities (w.r.t. Sizes)'
    )
    plt.xlabel('Community Size')
    plt.ylabel('Number of Communities')
    plt.tight_layout()
    plt.savefig(os.path.join(PATH_TO_IMG, f'community_size_{title}.pdf'))
    plt.show()

    # create a heatmap for the labels in the communities
    # the rows are the communities, the columns are the labels and the values are the number of members in the community
    # with the given label

    # get all the distinct labels
    distinct_labels = labels.unique()
    # create a dataframe with the community as index and the labels as columns
    # the values are the number of members in the community with the given label
    data = pd.DataFrame(
        index=community_sizes.index,
        columns=distinct_labels,
        data=[
            labels[communities == community].value_counts()
            for community in community_sizes.index
        ]
    )
    # fill the missing values with 0
    data.fillna(0, inplace=True)
    normalized_data = data.div(community_sizes, axis=0)

    # sort the communities by each column in descending order
    normalized_data = normalized_data.sort_values(
        by=list(normalized_data.columns),
        ascending=False
    )

    plt.figure(figsize=(10, 10))
    sns.heatmap(normalized_data, annot=False, cmap=sns.color_palette("Greens"))
    plt.yticks([])
    plt.xlabel('Label')
    plt.ylabel('Community')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig(os.path.join(PATH_TO_IMG, f'community_heatmap_{title}.pdf'))
    plt.show()


def plot_degree_distribution(graph: nx.classes.graph.Graph, title: str, plot: bool=False) -> None:
    """Plots the degree distribution of a graph.

    Parameters
    ----------
    graph : networkx.classes.graph.Graph
        The graph object.
    title : str
        The title of the plot.
    plot : bool
        Whether to plot the degree distribution.
    """

    schemes = ['loglog', 'loglinear']
    alpha_value = None

    for scheme in schemes:
        # get the degree distribution
        degree_distribution = pd.Series(dict(graph.degree)).value_counts()

        if plot:
            # plot the degree distribution as a scatter plot using the log-log scale
            plt.scatter(degree_distribution.index, degree_distribution.values)

        if scheme == 'loglog':
            if plot:
                plt.xscale('log')
                plt.yscale('log')
            p = np.polyfit(np.log(degree_distribution.index), np.log(degree_distribution.values), 1)
            label = f'Power Law: {-p[0]:.2f}'
            y = np.exp(p[1]) * degree_distribution.index ** p[0]
        else:
            if plot:
                plt.xscale('linear')
                plt.yscale('log')
            p = np.polyfit(degree_distribution.index, np.log(degree_distribution.values), 1)
            label = f'Exponential: {-p[0]:.2f}'
            y = np.exp(p[1]) * degree_distribution.index ** p[0]
        if plot:
            plt.xlabel('Degree')
            plt.ylabel('Number of Nodes')

        # calculate the slope of the line of best fit using np
        # the slope is the exponent of the power law
        if scheme == 'loglog':
            alpha_value = -p[0]

        if plot:
            # plot the line of best fit
            plt.plot(
                degree_distribution.index,
                y,
                color='red',
                label=label
            )
            plt.legend()

            plt.tight_layout()
            plt.savefig(os.path.join(PATH_TO_IMG, 'degree_dist', scheme, f'degree_dist_{scheme}_{title}.pdf'))
            plt.show()

    return alpha_value


def plot_category_sankey_diagram(graph_ids: list[int], skip_diagram=True) -> None:
    """ Plots a Sankey diagram for the evolution of the main genres of the users, over the years.
    """

    # for each graph, get the Label of the user
    # and for each user make a dictionary with the year as key and the Label as the value
    num_steps = len(graph_ids)

    # read the node list from ../gd/new_node_list/Interval={graph_id-1970}/part-00000-*.csv
    # and get the Label of the user
    # and for each user make a dictionary with the year as key and the Label as the value
    user_labels__ = {}
    for graph_id in graph_ids:
        user_labels__[graph_id] = {}
        with open(os.path.join(PATH_TO_NODE_LIST, f'Interval={graph_id-1970}', f"{NODE_LIST_P_0_95_DEFAULT}.csv"), 'r') as f:
            for i, line in enumerate(f):
                if i == 0:
                    continue
                user_id, label, _ = line.split(';')
                user_labels__[graph_id][user_id] = label

    # create a set of all the users in all the graphs
    users = set()
    for graph_id in graph_ids:
        users.update(user_labels__[graph_id].keys())

    # create a dictionary with the user as key and a dictionary with the year as key and the Label as value
    user_labels = {
        user: {
            graph_id: f"{user_labels__[graph_id][user] if user in user_labels__[graph_id] else 'None'}_{graph_id}"
            for graph_id, user_info in user_labels__.items()
        }
        for user in users
    }

    # create a dataframe with the user as index and the year as columns
    # the values are the Label of the user in the given year
    user_labels = pd.DataFrame(user_labels).T

    # write the dataframe to a csv file
    user_labels.to_csv(
        os.path.join(PATH_TO_CACHE, 'for_alluvial', f'user_labels_{graph_ids[0]}_{graph_ids[-1]}.csv'), sep=';'
    )

    # python does not have the facilities for this one...
    # so I will do it in R :)
    if not skip_diagram:
        # remove all the users which have a None label in all the years
        user_labels = user_labels[user_labels.apply(lambda row: row.str.contains('None').sum() < num_steps // 2, axis=1)]

        # get all the unique labels
        labels = list(set.union(*[set(user_labels[year]) for year in user_labels.columns]))
        labels.sort()

        print(labels)

        # convert the labels to indices in the labels list
        user_labels = user_labels.applymap(lambda label: labels.index(label))

        # make the sources list by taking the flatmap of the user_labels dataframe into a list
        # the sources are the value of the column in each year
        sources = np.array([user_labels.values[:, col] for col in range(user_labels.shape[1] - 1)]).flatten()
        targets = np.array([user_labels.values[:, col] for col in range(1, user_labels.shape[1])]).flatten()
        src_tgt, val = zip(*Counter(list(zip(sources, targets))).items())
        src, tgt = zip(*src_tgt)

        # use this dataframe to create a sanky diagram with the evolution of the main genres of the users
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=(small_labels := list(map(lambda label: label.split('_')[0], labels))),
                color=[f"rgb({r},{g},{b})" for r, g, b, _ in 255 * plt.cm.tab20(np.linspace(0, 1, len(small_labels)))]
            ),
            link=dict(
                source=src,
                target=tgt,
                value=val,
                label=None
            ))])
        fig.update_layout(
            autosize=False,
            width=1500,
            height=500,
        )
        fig.show()
        fig.write_image(
            os.path.join(PATH_TO_IMG, 'temporal' f'sankey_diagram_main_category_{str(graph_ids[0])}-{str(graph_ids[-1])}.pdf')
        )


def plot_community_sankey_diagram(graph_ids: list[int], graphs: list[nx.classes.Graph], skip_diagram=True) -> None:
    """
        Plots a Sankey diagram for the evolution of the main genres of the users, over the years.
    """

    # for each graph, get the Label of the user
    # and for each user make a dictionary with the year as key and the Label as the value

    # get the unique users in all the graphs
    users = set.union(*[set(graph.nodes) for graph in graphs])

    # create a dictionary with the user as key and a dictionary with the year as key and the community as value
    user_labels = {
        user: {
            graph_id: f"{(graph.nodes[user]['community'] if user in graph.nodes else 'None')}_{graph_id}"
            for graph_id, graph in zip(graph_ids, graphs)
        }
        for user in users
    }

    # create a dataframe with the user as index and the year as columns
    # the values are the Label of the user in the given year
    user_labels = pd.DataFrame(user_labels).T

    # write the dataframe to a csv file
    user_labels.to_csv(
        os.path.join(PATH_TO_CACHE, 'for_alluvial', f'user_communities_{graph_ids[0]}_{graph_ids[-1]}.csv'))

    # python does not have the facilities for this one...
    # so I will do it in R :)
    if not skip_diagram:
        # remove all the users which have a None label in all the years
        user_labels = user_labels[user_labels.apply(lambda row: row.str.contains('None').sum() == 0, axis=1)]

        # get all the unique labels
        labels = list(set.union(*[set(user_labels[year]) for year in user_labels.columns]))

        # convert the labels to indices in the labels list
        user_labels = user_labels.applymap(lambda label: labels.index(label))

        # make the sources list by taking the flatmap of the user_labels dataframe into a list
        # the sources are the value of the column in each year
        sources = np.array([user_labels.values[:, col] for col in range(user_labels.shape[1] - 1)]).flatten()
        targets = np.array([user_labels.values[:, col] for col in range(1, user_labels.shape[1])]).flatten()
        src_tgt, val = zip(*Counter(list(zip(sources, targets))).items())
        src, tgt = zip(*src_tgt)

        # use this dataframe to create a sankey diagram with the evolution of the main genres of the users
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=(small_labels := list(map(lambda label: label.split('_')[0], labels))),
                color=[f"rgb({r},{g},{b})" for r, g, b, _ in 255 * plt.cm.tab20(np.linspace(0, 1, len(user_labels)))]
            ),
            link=dict(
                source=src,
                target=tgt,
                value=val,
                label=None
            ))])
        fig.update_layout(
            autosize=False,
            width=500,
            height=500,
        )
        fig.show()
        fig.write_image(
            os.path.join(PATH_TO_IMG, f'sankey_diagram_community_{str(graph_ids[0]+1970)}-{str(graph_ids[-1]+1970)}.pdf')
        )



def get_graph_metrics(g_idx: int, graph: nx.classes.Graph, use_cache: bool, recalculate_communities: bool) -> dict[
    str, float]:
    """Calculates graph metrics for a graph.

    Parameters
    ----------
    g_idx : int
        The index of the graph.
    graph : nx.classes.Graph
        The graph to calculate the metrics for.
    use_cache: bool
        Whether to use the cached metrics or not.
    recalculate_communities: bool
        Whether to recalculate the communities or not.
    """

    # check if the metrics have already been calculated in the cache folder
    if not use_cache or not (metrics := read_metric_cache(g_idx)):
        print(f'Cache not found or cache intended to be recalculated: calculating metrics for graph {g_idx}...')

        print('Number of nodes:', graph.number_of_nodes())
        print('Number of edges:', graph.number_of_edges())

        if graph.number_of_edges() == 0:
            print('Graph has no edges, skipping...')
            return {
                'graph_id': g_idx,
                'num_nodes': graph.number_of_nodes(),
                'num_edges': graph.number_of_edges()
            }

        print('Finding Communities...')
        calculate_community_id(graph, override=recalculate_communities)
        print('Done.')

        print('Finding Clustering Coefficient...')
        node_clustering = list(nx.clustering(graph).values())
        mean_clustering = np.mean(node_clustering)
        std_clustering = np.std(node_clustering)
        print('Done.')

        print('Finding Entropy...')
        mean_entropy, std_entropy = get_average_entropy(graph)
        print('Done.')

        metrics = {
            'number_of_nodes': graph.number_of_nodes(),
            'number_of_edges': graph.number_of_edges(),
            'number_of_components': len(list(nx.algorithms.connected_components(graph))),
            'average_clustering': mean_clustering,
            'std_clustering': std_clustering,
            'assortativity_coefficient': get_assortativity_coefficient(graph),
            'number_of_communities': len(set(nx.get_node_attributes(graph, 'community').values())),
            'total_number_of_books_reviewed': sum(
                [graph.nodes[user]['Reviewed'] for user in graph.nodes]
            ),
            'average_number_of_books_reviewed_per_user': np.mean(
                [graph.nodes[user]['Reviewed'] for user in graph.nodes]
            ),
            'average_number_of_books_reviewed_per_community': np.mean(
                [
                    sum([graph.nodes[user]['Reviewed'] for user in graph.nodes if
                         graph.nodes[user]['community'] == community])
                    for community in set(nx.get_node_attributes(graph, 'community').values())
                ]
            ),
            'average_entropy': mean_entropy,
            'std_entropy': std_entropy
        }
        # save the graph as a gpickle using networkx
        nx.write_gpickle(graph, os.path.join(PATH_TO_CACHE, GRAPH_DIR, f'graph_{g_idx}.gpickle'))
        # save the metrics as a json file
        write_metrics_to_cache(g_idx, metrics)

    return metrics


def write_metrics_to_cache(g_idx, metrics):
    with open(os.path.join(PATH_TO_CACHE, METRIC_DIR, f'graph_metrics_{g_idx}.json'), 'w') as f:
        json.dump(metrics, f)


def read_metric_cache(g_idx):
    if os.path.exists(os.path.join(PATH_TO_CACHE, METRIC_DIR, f'graph_metrics_{g_idx}.json')):
        print(f'Loading graph metrics for graph {g_idx} from cache.')
        with open(os.path.join(PATH_TO_CACHE, METRIC_DIR, f'graph_metrics_{g_idx}.json'), 'r') as f:
            metrics = json.load(f)
        return metrics
    return False


def plot_metrics_over_years(g_idx_list: list[int]):
    """Plots the metrics over the years.

    Parameters
    ----------
    g_idx_list : list[int]
        The indices of the graphs to plot the metrics for.
    """

    # get the metrics for each graph
    years = list(map(lambda g_idx: g_idx, g_idx_list))
    ticks = years, list(map(lambda year: str(year)[2:], years))
    metrics = [read_metric_cache(g_idx) for g_idx in g_idx_list]

    if not all(metrics):
        print(f'Cache not found for all graphs')
        return

    # get the number of nodes, edges and components for each graph
    num_nodes = list(map(lambda metric: metric['number_of_nodes'], metrics))
    num_edges = list(map(lambda metric: metric['number_of_edges'], metrics))
    num_components = [metric['number_of_components'] for metric in metrics]

    # get the average clustering coefficient and assortativity coefficient for each graph
    avg_clustering = [metric['average_clustering'] for metric in metrics]
    std_clustering = [metric['std_clustering'] for metric in metrics]
    assortativity_coefficient = [metric['assortativity_coefficient'] for metric in metrics]
    avg_entropy = [metric['average_entropy'] for metric in metrics]
    std_entropy = [metric['std_entropy'] for metric in metrics]

    plot_simple_trend(
        years, num_components,
        x_ticks=ticks, save_name='num_components', trend_label='Number of Components', y_label='Number of Components'
    )

    plot_simple_trend(
        years, num_nodes,
        x_ticks=ticks, save_name='num_nodes', trend_label='Number of Nodes', y_label='Number of Nodes'
    )

    plot_simple_trend(
        years, num_edges,
        x_ticks=ticks, save_name='num_edges', trend_label='Number of Edges', y_label='Number of Edges'
    )

    plot_simple_trend(
        years, avg_clustering,
        x_ticks=ticks, save_name='avg_clustering', trend_label='Average Clustering', y_label='Average Clustering',
        error=std_clustering
    )

    plot_simple_trend(
        years, assortativity_coefficient,
        x_ticks=ticks, save_name='assortativity_coefficient', trend_label='Assortativity Coefficient',
        y_label='Assortativity Coefficient'
    )

    plot_simple_trend(
        years, avg_entropy,
        x_ticks=ticks, save_name='avg_entropy', trend_label='Average Entropy', y_label='Average Entropy',
        error=std_entropy
    )

    # read the number of books reviewed per year from ../gd/number_of_books.csv which is ';' separated with header
    # and the first column is the year_from_1970 and the second column is the number of books reviewed with column name
    # named books
    num_books = pd.read_csv(os.path.join(PATH_TO_DATA, 'number_of_books.csv'), sep=';', header=0)
    # add 1970 to the year_from_1970 column to get the actual year
    num_books['year'] = num_books['year_from_1970'] + 1970

    num_books = num_books.set_index('year')
    # get only the books which is in the years we are interested in
    num_books = num_books.loc[g_idx_list]['books']

    years, num_books = zip(*sorted(zip(years, num_books)))

    # plot the number of books reviewed per year
    plot_simple_trend(
        years, num_books,
        x_ticks=ticks, save_name='num_books', trend_label='Number of Books Reviewed', y_label='Number of Books Reviewed'
    )


def get_comm_sizes(graph: nx.classes.Graph) -> list[int]:
    """Returns the sizes of the communities in the graph.

    Parameters
    ----------
    graph : nx.classes.Graph
        The graph to get the community sizes for.

    Returns
    -------
    list[int]
        The sizes of the communities in the graph.
    """

    # get the communities in the graph, from the node data
    communities = pd.Series([
        node_data['community'] for _, node_data in graph.nodes(data=True)
    ])
    # get the sizes of the communities
    comm_sizes = communities.value_counts().sort_index().tolist()
    # filter out the communities with size < 3
    comm_sizes = list(filter(lambda size: size >= 3, comm_sizes))
    return comm_sizes


def entropy_distribution_over_years(g_idx_list: list[int]):
    """Plots the entropy distribution over the years.

    Parameters
    ----------
    g_idx_list : list[int]
        The indices of the graphs to plot the metrics for.
    """

    # get the metrics for each graph
    graphs = [read_graph_from_file(g_idx) for g_idx in g_idx_list]
    years = list(map(lambda g_idx: g_idx, g_idx_list))

    # get the entropy for each graph
    entropies = [(year, get_entropies(graph)) for year, graph in zip(years, graphs)]
    comm_sizes = [(year, get_comm_sizes(graph)) for year, graph in zip(years, graphs)]

    x, y = zip(*comm_sizes)
    y = list(map(lambda coms_s: np.mean(np.log10(coms_s)), y))

    plot_simple_trend(
        x, y, x_ticks=(x, list(map(lambda year: str(year)[2:], x))),
        save_name='comm_sizes', trend_label='Community Sizes', y_label='mean log community size'
    )

    # convert to a dataframe
    df_e = pd.DataFrame(entropies, columns=['year', 'entropies'])
    df_s = pd.DataFrame(comm_sizes, columns=['year', 'sizes'])

    # convert the years to a categorical variable
    df_e['year'] = pd.Categorical(df_e['year'], categories=years)
    df_s['year'] = pd.Categorical(df_s['year'], categories=years)

    # explode the entropies into a separate row for each entropy
    df_e = df_e.explode('entropies').reset_index(drop=True)
    df_s = df_s.explode('sizes').reset_index(drop=True)

    # save the dataframes
    df_e.to_csv(os.path.join(PATH_TO_CACHE, 'for_ridge', 'entropies.csv'), index=True)
    df_s.to_csv(os.path.join(PATH_TO_CACHE, 'for_ridge', 'comm_sizes.csv'), index=True)

    # python does not have the facilities for this one...
    # we will use R instead, :)


def plot_simple_trend(x, y, x_ticks, save_name, trend_label=None, x_label="Year", y_label="Coefficient", error=None):
    """Plots a simple trend line.
    """

    sns.set_theme()
    fig, ax = plt.subplots(figsize=(10, 6))
    y = np.array(y)
    sns.lineplot(x=x, y=y, ax=ax, label=trend_label)

    if error is not None:
        # below the line plot make a line plot with the error
        sns.lineplot(x=x, y=y - error, ax=ax, color='red', alpha=0.5)
        sns.lineplot(x=x, y=y + error, ax=ax, color='red', alpha=0.5)

    ax.set_xticks(x_ticks[0])
    ax.set_xticklabels(x_ticks[1])
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    fig.savefig(os.path.join(TREND_DIR, f'{save_name}_{x_ticks[0][0]}_{x_ticks[0][-1]}.pdf'), bbox_inches='tight')
    plt.show()


def find_graph_metrics(use_cache: bool = True, recalculate_communities: bool = False):
    for year in tqdm(range(1998, 2019), desc='Finding Graph Metrics...', position=0, leave=True, unit='year'):
        print(get_graph_metrics(year, read_graph_from_file(year),
                                use_cache=use_cache, recalculate_communities=recalculate_communities
                                ))


def alpha_vales_table():
    alpha_values = []
    for year in tqdm(range(2000, 2018), desc='Finding Graph Metrics...', position=0, leave=True, unit='year'):
        alpha_values.append((year, plot_degree_distribution(read_graph_from_file(year), year)))
    # create a latex table of the alpha values horizontally using pandas
    df = pd.DataFrame(alpha_values, columns=['year', 'alpha'])
    df['year'] = df['year'].astype(int)
    df['alpha'] = df['alpha'].round(3)
    df = df.T
    df = df.reset_index()
    print(df.to_latex(index=False, header=False))


if __name__ == '__main__':
    import time
    from tqdm import tqdm

    s_time = time.time()
    # find_graph_metrics(use_cache=False, recalculate_communities=True)
    # plot_metrics_over_years([i for i in range(2000, 2018)])
    # entropy_distribution_over_years([i for i in range(2000, 2018)])
    # alpha_vales_table()

    ranges = [range(2000, 2005), range(2004, 2010), range(2009, 2015), range(2014, 2018)]
    for year_range in ranges:
        years = [i for i in year_range]
        # gs = [read_graph_from_file(i) for i in years]

        plot_category_sankey_diagram(years)
        # plot_community_sankey_diagram(years, gs)
        # input('Press enter to continue...')

    # convert time to minutes and seconds
    minutes, seconds = divmod(time.time() - s_time, 60)
    print(f'Total time: {minutes:.0f} minutes and {seconds:.0f} seconds')
