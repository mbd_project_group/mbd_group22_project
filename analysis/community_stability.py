import os
import re
from pprint import pprint

import numpy as np


# the directory for the data is "./cache/user_communities_*.csv" and "./cache/user_labels_*.csv"

def read_user_communities(file_prefix, delim=","):
    """Reads the user communities from the cache directory.
    Returns
    -------
    user_communities : dict
        A dictionary mapping user ids to community ids.
    """
    user_communities = {}
    for filename in os.listdir("./cache/for_alluvial"):
        if filename.startswith(file_prefix):
            with open(os.path.join("./cache/for_alluvial", filename), "r") as f:
                for i, line in enumerate(f):
                    # the first line is the header containing the reviewer_id and then the years for which we have data
                    if i == 0:
                        # initialize the user_communities dictionary
                        temp_communities = {
                            year_id: {} for year_id in line.strip().split(delim)[1:]
                        }
                    else:
                        # split the line into reviewer_id and the community ids for each year
                        line = line.strip().split(delim)
                        reviewer_id = line[0]
                        # add the reviewer_id to the matching community for each year
                        for year_id, community_id in zip(temp_communities.keys(), line[1:]):
                            if community_id not in temp_communities[year_id]:
                                temp_communities[year_id][community_id] = set()
                            temp_communities[year_id][community_id].add(reviewer_id)
                # add the communities for each year to the user_communities dictionary
                for year_id, communities in temp_communities.items():
                    user_communities[year_id] = communities
    for year_id, communities in user_communities.items():
        print(year_id, communities.keys())
    return user_communities


def find_core_path(user_communities):
    # find the set of communities in each year
    community_sets = {}
    for year_id, communities in user_communities.items():
        community_sets[year_id] = set(communities.keys())

    print(dict(map(lambda x: (x[0], len(x[1])), community_sets.items())))

    # for each community in each year find the following two values:
    # 1. max number of users that are retained in the community in the next year
    # 2. the minimum number of this max users that are retained over all years
    community_stability = {
        year_id: {community_id: [-1, -1, -1, -1] for community_id in communities}
        for year_id, communities in user_communities.items()
    }

    # iterate over all years
    for year_id, communities in user_communities.items():
        # iterate over all communities in the current year
        for community_id, users in communities.items():
            if 'None' in community_id or 'nan_' in community_id or 'Books_' in community_id:
                continue
            # iterate over all years after the current year
            next_year_id = str(int(year_id) + 1)
            if next_year_id in user_communities:
                # iterate over all communities in the next year
                for next_community_id, next_users in user_communities[next_year_id].items():
                    if 'None' in next_community_id or 'nan_' in next_community_id or 'Books_' in next_community_id:
                        continue
                    # find the number of users that are retained in the community
                    retained_users = len(users.intersection(next_users))
                    # update the max number of users retained in the community
                    if retained_users > community_stability[year_id][community_id][0]:
                        community_stability[year_id][community_id][0] = retained_users / len(users)
                        community_stability[year_id][community_id][1] = len(users - next_users) / len(users)
                        community_stability[year_id][community_id][2] = len(next_users - users) / len(next_users)
                        community_stability[year_id][community_id][3] = next_community_id

    # filter out communities that have -1
    community_stability = {
        year_id: {
            community_id: {
                "retained": community_stability[year_id][community_id][0],
                "lost": community_stability[year_id][community_id][1],
                "gained": community_stability[year_id][community_id][2],
                "next_community_id": community_stability[year_id][community_id][3]
            }
            for community_id in communities
            if community_stability[year_id][community_id][0] > 0
        }
        for year_id, communities in user_communities.items()
    }

    return community_stability


def convert_to_path_list(community_stability):
    # convert the community_stability dictionary to a list of paths
    paths = []
    for year_id, communities in community_stability.items():
        for community_id, community in communities.items():
            path = [community_id]
            path_year_id = year_id
            next_community_id = community["next_community_id"]
            while True:
                path.append(next_community_id)
                path_year_id = str(int(path_year_id) + 1)
                if path_year_id in community_stability:
                    if next_community_id in community_stability[path_year_id]:
                        next_community_id = community_stability[path_year_id][next_community_id][
                            "next_community_id"
                        ]
                    else:
                        path.append("dissolved")
                        break
                else:
                    break
            paths.append(path)
    return paths


def plot_paths(community_stability):
    # make a bar plot with error bars for each year
    # the x-axis is the year
    # the y-axis is the fraction of retained users
    # the error bars are the standard deviation of the retained users
    import pandas as pd
    import seaborn as sns
    import matplotlib.pyplot as plt

    # plot a violin plot for each year on the same plot
    plt.figure(figsize=(10, 5))
    sns.set_style("whitegrid")
    sns.set_context("paper", font_scale=2)
    sns.set_palette("colorblind")

    data = pd.DataFrame(
        [
            {
                "year": int(year_id),
                "retained": community["retained"],
                "lost": community["lost"],
                "gained": community["gained"],
            }
            for year_id, communities in community_stability.items()
            for community_id, community in communities.items()
        ]
    )

    agg_data = data.groupby("year").agg({"retained": ["mean", "std"], "lost": ["mean", "std"], "gained": ["mean", "std"]})
    agg_data.columns = ["_".join(x) for x in agg_data.columns.ravel()]
    agg_data = agg_data.reset_index()

    agg_data.plot(
        x="year",
        y="retained_mean",
        kind="bar",
        color="blue",
        ax=plt.gca(),
        width=0.8,
        alpha=0.7,
        label="Retained",
    )
    plt.gca().set_xticklabels(agg_data["year"].values, rotation=45)
    plt.gca().set_ylabel("Fraction of users")
    plt.gca().set_xlabel("Year")
    plt.tight_layout()
    plt.savefig("./images/trends/retained_users.pdf")
    plt.show()


def plot_mixing_user_level(user_community):
    import seaborn as sns
    import matplotlib.pyplot as plt

    per_year = {}
    genres = set()
    for year, communities in user_community.items():
        per_year[year] = {}
        for community, users in communities.items():
            for user in users:
                genre = community.split("_")[0]
                per_year[year][user] = genre
                genres.add(genre)

    # for each user make a list of genres they read in each year
    user_genres = {}
    for year, users in per_year.items():
        for user, genre in users.items():
            if user not in user_genres:
                user_genres[user] = []
            user_genres[user].append(genre)

    genres = list(sorted(genres))

    heatmap = np.zeros((len(genres), len(genres)))
    for user, u_genre_list in user_genres.items():
        # iterate over u_genre_list and count the number of times each genre is followed by another genre
        for i in range(len(u_genre_list) - 1):
            heatmap[genres.index(u_genre_list[i]), genres.index(u_genre_list[i + 1])] += 1

    # drop the row and column associated with the "None" genre
    # get the index of the "None" genre
    none_index = genres.index("None")
    # drop the row and column associated with the "None" genre
    heatmap = np.delete(heatmap, none_index, axis=0)
    heatmap = np.delete(heatmap, none_index, axis=1)
    # drop the "None" genre from the list of genres
    genres = [genre for genre in genres if genre != "None"]

    # shorten the genre names by enumerating them and then add a legend to the plot
    genre_names = {
        i: genre for i, genre in enumerate(genres)
    }
    genres = [i for i in range(len(genres))]

    # print the genre names as a latex table using pandas
    import pandas as pd
    df = pd.DataFrame(genre_names, index=[0])
    df = df.transpose()
    df.columns = ["Genre"]
    df.index.name = "Genre ID"
    print(df.to_latex(index=True))

    # normalize the heatmap row wise
    heatmap = heatmap / heatmap.sum(axis=1, keepdims=True)

    # plot the heatmap
    plt.figure(figsize=(10, 10))
    sns.set_style("whitegrid")
    sns.set_context("paper", font_scale=2)
    sns.set_palette("colorblind")
    sns.heatmap(heatmap, annot=False, fmt=".2f", xticklabels=genres, yticklabels=genres,
                cmap="Greens")
    plt.tight_layout()
    plt.savefig("./images/trends/user_mixing_user_level.pdf")
    plt.show()


def plot_mixing(community_stability):
    import pandas as pd
    import seaborn as sns
    import matplotlib.pyplot as plt

    # for all the keys in community_stability, find the year and the sub category
    per_year = {}
    for year, genre_details in community_stability.items():
        year = int(year)
        if year not in per_year:
            per_year[year] = {}
        for genre, details in genre_details.items():
            genre = genre.split("_")[0]
            if genre not in per_year[year]:
                per_year[year][genre] = details["next_community_id"].split("_")[0]
    pprint(per_year)

    # get all unqiue genres
    genres = set()
    for year, genre_details in per_year.items():
        for genre in genre_details:
            genres.add(genre)

    genres = list(sorted(genres))

    heatmap = np.array([[0 for _ in range(len(genres))] for _ in range(len(genres))])

    for year, genre_details in per_year.items():

        # fill in the heatmap by counting the number of times a genre is followed by another genre
        for i, genre in enumerate(genres):
            for j, genre2 in enumerate(genres):
                if genre in per_year[year] and per_year[year][genre] == genre2:
                    heatmap[i, j] += 1

    # normalize the heatmap row wise
    heatmap = heatmap / heatmap.sum(axis=1, keepdims=True)

    # plot the heatmap
    plt.figure(figsize=(10, 10))
    sns.set_style("whitegrid")
    sns.set_context("paper", font_scale=2)
    sns.heatmap(heatmap,
                annot=False, fmt="d", xticklabels=genres, yticklabels=genres, linewidths=0.5, linecolor="yellow",
                cmap=sns.diverging_palette(220, 20, as_cmap=True, center="light"))
    plt.legend()
    plt.tight_layout()
    plt.savefig("./images/trends/mixing.pdf")
    plt.show()


def blah(file_prefix):
    """Reads the user communities from the cache directory.
    Returns
    -------
    user_communities : dict
        A dictionary mapping user ids to community ids.
    """
    user_to_community = {}
    for filename in os.listdir("./cache/for_alluvial"):
        if filename.startswith(file_prefix):
            with open(os.path.join("./cache/for_alluvial", filename), "r") as f:
                for i, line in enumerate(f):
                    # the first line is the header containing the reviewer_id and then the years for which we have data
                    if i == 0:
                        # initialize the user_communities dictionary
                        years = line.strip().split(";")[1:]
                    else:
                        # split the line into reviewer_id and the community ids for each year
                        line = line.strip().split(";")
                        reviewer_id = line[0]
                        communities = line[1:]

                        if reviewer_id not in user_to_community:
                            user_to_community[reviewer_id] = {}
                        for year, community in zip(years, communities):
                            user_to_community[reviewer_id][year] = community

    # filter out the users that have None_* matched as regex in any year
    user_to_community = {
        user_id: communities
        for user_id, communities in user_to_community.items()
        if sum("None" in community for community in communities.values()) < 4
    }
    return user_to_community


if __name__ == '__main__':
    # u_communities = read_user_communities("user_communities_")
    # _paths = find_core_path(u_communities)
    _paths = read_user_communities("user_labels_", delim=";")
    plot_mixing(find_core_path(_paths))