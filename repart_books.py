# python2.7 project.py

from pyspark.sql import SparkSession


# create spark session
spark = SparkSession.builder.appName('repart_books').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')


spark.read\
    .json('/user/s2236141/group22/meta_Books.json.gz')\
    .drop(
        'details', 'tech1', 'tech2', 'fit', 'feature', 'image', 'similar_item', 'description',
        'imageURL', 'imageURLHighRes', 'date', 'rank'
    )\
    .repartition(10)\
    .write.json('/user/s2236141/group22/books', mode='overwrite', compression='gzip')
