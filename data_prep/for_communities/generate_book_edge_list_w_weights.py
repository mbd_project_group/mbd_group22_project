import argparse

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, ArrayType
from pyspark.sql.functions import size, array_intersect, col

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# define the review schema
review_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('sub_category', StringType(), True),
    StructField('reviewers', ArrayType(StringType()), True)
])


parser = argparse.ArgumentParser()
parser.add_argument("--year", help="the year for which to make the edge list. (range from 26-48 inclusive)")

args = parser.parse_args()

year = args.year

edge_df = spark.read.json('/user/s2236141/group22/edges/year_from_1970={}/*.json'.format(year), schema=review_schema)

# give alias to the columns of edge_df
other_edge_df = edge_df.withColumnRenamed('asin', 'asin_1') \
    .withColumnRenamed('sub_category', 'sub_category_1') \
    .withColumnRenamed('reviewers', 'reviewers_1')

# for every pair of books in the same year, get the number of reviewers who have reviewed both books
# and then return a list of tuples of the form (book1, book2, number of reviewers)
edge_w_weight_df = edge_df.crossJoin(other_edge_df)

edge_w_weight_df = edge_w_weight_df \
    .filter(col('asin') < col('asin_1')) \
    .withColumn('num_reviewers', size(array_intersect(col('reviewers'), col('reviewers_1')))) \
    .select('asin', 'sub_category', 'asin_1', 'sub_category_1', 'num_reviewers') \
    .filter(col('num_reviewers') > 0) \
    .repartition(10)

# write the results to disk as a csv file
edge_w_weight_df.write.csv('/user/s2236141/group22/edges_w_weight/edge_list_{}'.format(year), mode='overwrite')
