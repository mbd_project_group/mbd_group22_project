from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, LongType, BooleanType
from pyspark.sql.functions import size, col

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

"""
# format of amazon reviews record on books
# root
#  |-- asin: string (nullable = true)
#  |-- overall: double (nullable = true)
#  |-- reviewerID: string (nullable = true)
#  |-- unixReviewTime: long (nullable = true)
#  |-- verified: boolean (nullable = true)
#  |-- author: string (nullable = true)
#  |-- has_amazon_author_page: boolean (nullable = true)
#  |-- top_category: string (containsNull = true)
#  |-- sub_category: string (containsNull = true)
#  |-- fine_grained_category: string (containsNull = true)
"""

# define the review schema
review_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('overall', DoubleType(), True),
    StructField('reviewerID', StringType(), True),
    StructField('unixReviewTime', LongType(), True),
    StructField('year_from_1970', LongType(), True),
    StructField('verified', BooleanType(), True),
    StructField('author', StringType(), True),
    StructField('has_amazon_author_page', BooleanType(), True),
    StructField('top_category', StringType(), True),
    StructField('sub_category', StringType(), True),
    StructField('fine_grained_category', StringType(), True)
])

# read the joined_reviews data
reviews_df = spark.read.json('/user/s2236141/group22/joined_reviews/*.json.gz', schema=review_schema)

# for every year_from_1970, get the books in that year and then
# get the list of users who have reviewed that book

edge_df = reviews_df.select('year_from_1970', 'reviewerID', 'asin', 'sub_category') \
    .groupBy('year_from_1970', 'asin', 'sub_category') \
    .agg({'reviewerID': 'collect_set'}) \
    .withColumnRenamed('collect_set(reviewerID)', 'reviewers')

# filter out the books with less than 10 reviews
edge_df = edge_df.filter(size(edge_df.reviewers) >= 10).repartition(10, 'year_from_1970')


# repartition the data with respect to year_from_1970
# and save the data into a csv file dropping the year column
edge_df.write.partitionBy('year_from_1970').json('/user/s2236141/group22/edges', mode='overwrite', compression='gzip')
