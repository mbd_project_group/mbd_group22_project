from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, LongType, BooleanType
from pyspark.sql.functions import col, size, array_intersect, flatten, array_distinct, collect_set

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

"""
# format of amazon reviews record on books
# root
#  |-- asin: string (nullable = true)
#  |-- overall: double (nullable = true)
#  |-- reviewerID: string (nullable = true)
#  |-- year_from_1970: long (nullable = true)
#  |-- sub_category: string (containsNull = true)
"""

# define the review schema
review_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('reviewerID', StringType(), True),
    StructField('year_from_1970', LongType(), True),
    StructField('sub_category', StringType(), True),
])

# read the joined_reviews data and convert year_from_1970 to date using 1970+year_from_1970-12-31:00:00:00
reviews_df = spark.read.json('/user/s2236141/group22/joined_reviews/*.json.gz', schema=review_schema)

# for every reviewer, get the count of the sub-categories of books they have reviewed
count_df = reviews_df.select('reviewerID', 'sub_category', 'asin', 'year_from_1970') \
    .groupBy('reviewerID', 'sub_category', 'year_from_1970') \
    .agg({'asin': 'count'}) \
    .withColumnRenamed('count(asin)', 'count') \
    .withColumnRenamed('sub_category', 'r_category')

# get the max count from all the sub-categories for each reviewer
max_count_df = count_df.groupBy('reviewerID', 'year_from_1970').agg({'count': 'max'}) \
    .withColumnRenamed('max(count)', 'count')

# get the sub-categories of books that the reviewer has reviewed the most, we choose the first one if there are multiple
# we filter out the reviewers who have only reviewed less than 5 books
# we also filter out the reviewers who have a p-value of 0.05 or less, calculated by
# the number of books reviewed in the max category divided by the total number of books reviewed
edge_df = count_df.join(max_count_df, ['reviewerID', 'year_from_1970', 'count']) \
    .select('reviewerID', 'r_category', 'year_from_1970', 'count') \
    .dropDuplicates(['reviewerID', 'year_from_1970']) \
    .join(reviews_df, ['reviewerID', 'year_from_1970']) \
    .select('reviewerID', 'r_category', 'asin', 'year_from_1970', 'count') \
    .filter(col('r_category') != 'Books') \
    .groupBy('year_from_1970', 'reviewerID', 'r_category') \
    .agg({'asin': 'collect_set'}) \
    .withColumnRenamed('collect_set(asin)', 'books') \
    .filter(size('books') >= 5) \
    .filter(col('count') / size(col('books')) >= 0.95)

edge_df.select('reviewerID', 'r_category', 'year_from_1970', 'books') \
    .withColumn('Reviewed', size('Books')) \
    .withColumnRenamed('reviewerID', 'Id') \
    .withColumnRenamed('r_category', 'Label') \
    .withColumnRenamed('year_from_1970', 'Interval') \
    .select('Id', 'Label', 'Interval', 'Reviewed') \
    .repartition(1, 'Interval') \
    .write.partitionBy('Interval').csv('/user/s2236141/group22/gd_1/node_list_filtered', mode='overwrite', header=True, sep=';')


edge_df.select('year_from_1970', 'books') \
    .groupBy('year_from_1970') \
    .agg(size(array_distinct(flatten(collect_set('books'))))) \
    .withColumnRenamed('size(array_distinct(flatten(collect_set(books))))', 'books') \
    .repartition(1).write.csv('/user/s2236141/group22/gd_1/number_of_books', mode='overwrite', header=True, sep=';')

edge_df = edge_df.select('reviewerID', 'year_from_1970', 'books')
other_edge_df = edge_df.select('reviewerID', 'books', 'year_from_1970') \
    .withColumnRenamed('year_from_1970', 'other_year_from_1970') \
    .withColumnRenamed('reviewerID', 'reviewerID_1') \
    .withColumnRenamed('books', 'books_1')

# for every pair of books in the same year, get the number of reviewers who have reviewed both books
# and then return a list of tuples of the form (book1, book2, number of reviewers)
edge_w_weight_df = edge_df.crossJoin(other_edge_df)

edge_w_weight_df = edge_w_weight_df \
    .filter(col('year_from_1970') == col('other_year_from_1970')) \
    .filter(col('reviewerID') < col('reviewerID_1')) \
    .withColumn('num_books', size(array_intersect(col('books'), col('books_1')))) \
    .select('year_from_1970', 'reviewerID', 'reviewerID_1', 'num_books') \
    .filter(col('num_books') >= 1) \
    .withColumnRenamed('year_from_1970', 'Interval') \
    .withColumnRenamed('num_books', 'Weight') \
    .withColumnRenamed('reviewerID', 'Source') \
    .withColumnRenamed('reviewerID_1', 'Target')

# write the results to disk as a csv file
edge_w_weight_df \
    .repartition(1, 'Interval') \
    .write.partitionBy('Interval').csv('/user/s2236141/group22/gd_1/edge_list_dense', mode='overwrite', header=True, sep=';')
