# python2.7 project.py

from pyspark.sql import SparkSession
from pyspark.sql.types import StructField, StringType, StructType, DoubleType, LongType, BooleanType
from pyspark.sql.functions import col, regexp_replace

"""
# format of amazon reviews file on books
# root
#  |-- asin: string (nullable = true)
#  |-- overall: double (nullable = true)
#  |-- reviewText: string (nullable = true)
#  |-- reviewerID: string (nullable = true)
#  |-- reviewerName: string (nullable = true)
#  |-- unixReviewTime: long (nullable = true)
#  |-- verified: boolean (nullable = true)
#  |-- vote: string (nullable = true)
"""

"""
# format of amazon books product metadata
# root
#  |-- asin: string (nullable = true)
#  |-- author: string (nullable = true)
#  |-- has_amazon_author_page: boolean (nullable = true)
#  |-- top_category: string (containsNull = true)
#  |-- sub_category: string (containsNull = true)
#  |-- fine_grained_category: string (containsNull = true)
"""

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# define the review schema
review_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('overall', DoubleType(), True),
    StructField('reviewerID', StringType(), True),
    StructField('unixReviewTime', LongType(), True),
    StructField('verified', BooleanType(), True),
])

# define the product metadata schema
product_metadata_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('author', StringType(), True),
    StructField('has_amazon_author_page', BooleanType(), True),
    StructField('top_category', StringType(), True),
    StructField('sub_category', StringType(), True),
    StructField('fine_grained_category', StringType(), True)
])

# read in the reviews data
reviews_with_year = spark.read.json('/user/s2236141/group22/cleaned_reviews/*.json.gz', schema=review_schema) \
    .withColumn('year_from_1970', (col('unixReviewTime') / 31536000).cast('int'))

books_df = spark.read.json('/user/s2236141/group22/books_cleaned/*.json.gz', schema=product_metadata_schema)

# find the category of the book for each review in the reviews dataframe
with_year_cat_df = reviews_with_year.join(books_df, on='asin', how='left')

# partition by year column
with_year_cat_df = with_year_cat_df.repartition(20)

# create a new df with three columns: asin, asin, year_from_1970
# then create an edge list for this user where the other users are the users who reviewed the same book
# and the weight of the edge is the number of books they reviewed together


# write the reviews to a json file in the hdfs directory /user/s2236141/group22/joined_reviews
with_year_cat_df.write.json('/user/s2236141/group22/joined_reviews', mode='overwrite', compression='gzip')
