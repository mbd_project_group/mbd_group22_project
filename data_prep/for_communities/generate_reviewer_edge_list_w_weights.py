from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, ArrayType, LongType
from pyspark.sql.functions import size, array_intersect, col

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# define the review schema
review_schema = StructType([
    StructField('year_from_1970', LongType(), True),
    StructField('reviewerID', StringType(), True),
    StructField('sub_category', StringType(), True),
    StructField('books', ArrayType(StringType()), True)
])

edge_df = spark.read.json('/user/s2236141/group22/reviewer_agg/*.json.gz', schema=review_schema)

# give alias to the columns of edge_df
other_edge_df = edge_df \
    .withColumnRenamed('year_from_1970', 'other_year_from_1970') \
    .withColumnRenamed('reviewerID', 'reviewerID_1') \
    .withColumnRenamed('sub_category', 'sub_category_1') \
    .withColumnRenamed('books', 'books_1')

# for every pair of books in the same year, get the number of reviewers who have reviewed both books
# and then return a list of tuples of the form (book1, book2, number of reviewers)
edge_w_weight_df = edge_df.crossJoin(other_edge_df)

edge_w_weight_df = edge_w_weight_df \
    .filter(col('year_from_1970') == col('other_year_from_1970')) \
    .filter(col('reviewerID') < col('reviewerID_1')) \
    .withColumn('num_books', size(array_intersect(col('reviewers'), col('reviewers_1')))) \
    .select('year_from_1970', 'reviewerID', 'sub_category', 'reviewerID_1', 'sub_category_1', 'num_books') \
    .filter(col('num_books') >= 5) \
    .withColumnRenamed('year_from_1970', 'Timestamp') \
    .withColumnRenamed('num_books', 'Weight') \
    .wtihColumnRenamed('reviewerID', 'Source') \
    .wtihColumnRenamed('reviewerID_1', 'Target') \
    .repartition(1)

# write the results to disk as a csv file
edge_w_weight_df.write.csv('/user/s2236141/group22/reviewer_edge_list', mode='overwrite', sep=';', header=True)
