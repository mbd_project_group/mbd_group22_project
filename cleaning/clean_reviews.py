# python2.7 project.py

from pyspark.sql import SparkSession
from pyspark.sql.types import StructField, StringType, StructType, DoubleType, LongType, BooleanType

"""
# format of amazon reviews file on books
# root
#  |-- asin: string (nullable = true)
#  |-- overall: double (nullable = true)
#  |-- reviewText: string (nullable = true)
#  |-- reviewerID: string (nullable = true)
#  |-- reviewerName: string (nullable = true)
#  |-- unixReviewTime: long (nullable = true)
#  |-- verified: boolean (nullable = true)
#  |-- vote: string (nullable = true)
"""

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# define the review schema
review_schema = StructType([
    StructField('asin', StringType(), True),
    StructField('overall', DoubleType(), True),
    StructField('reviewText', StringType(), True),
    StructField('reviewerID', StringType(), True),
    StructField('reviewerName', StringType(), True),
    StructField('unixReviewTime', LongType(), True),
    StructField('verified', BooleanType(), True),
    StructField('vote', StringType(), True)
])

# read in the reviews data
reviews = spark.read.json('/user/s2236141/group22/reviews/*.json.gz', schema=review_schema)

# drop the reviewerName, reviewText and vote columns
reviews = reviews.drop('reviewerName', 'reviewText', 'vote')

# remove reviews with asin is null, overall is null, unixReviewTime is null
reviews = reviews.filter(
    reviews.asin.isNotNull() & reviews.overall.isNotNull() & reviews.unixReviewTime.isNotNull() &
    reviews.reviewText.isNotNull() & reviews.reviewerID.isNotNull() & reviews.reviewerName.isNotNull()
)

# remove reviews where the string fields are empty
reviews = reviews.filter(
    (reviews.asin != '\\s*') & (reviews.reviewText != '\\s*') & (reviews.reviewerID != '\\s*')
)

# sort by unixReviewTime
reviews = reviews.sort('unixReviewTime')

# write the reviews to a json file in the hdfs directory /user/s2236141/group22/cleaned_reviews
reviews.write.json('/user/s2236141/group22/cleaned_reviews', mode='overwrite', compression='gzip')
