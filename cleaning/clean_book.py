# python2.7 project.py

from pyspark.sql import SparkSession
from pyspark.sql.types import StructField, StringType, ArrayType, StructType
from pyspark.sql.functions import regexp_extract, coalesce, element_at, when, regexp_replace

# create spark session
spark = SparkSession.builder.appName('project').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# define the product metadata schema
product_metadata_schema = StructType([
    StructField('also_buy', ArrayType(StringType(), True), True),
    StructField('also_view', ArrayType(StringType(), True), True),
    StructField('asin', StringType(), True),
    StructField('brand', StringType(), True),
    StructField('category', ArrayType(StringType(), True), True),
    StructField('main_cat', StringType(), True),
    StructField('price', StringType(), True),
    StructField('title', StringType(), True)
])

# read in the product metadata
product_metadata = spark.read.json('/user/s2236141/group22/books/*.json.gz', schema=product_metadata_schema)

# # read in the reviews
# reviews = spark.read.json('user/s2236141/group22/reviews/*.json.gz', schema=review_schema)

# filter rows with null asin, brand, category, title
product_metadata = product_metadata.filter(
    (product_metadata.asin.isNotNull()) & (~product_metadata.asin.rlike('^\\s*$')) &
    (product_metadata.brand.isNotNull()) & (~product_metadata.brand.rlike('^\\s*$')) &
    (product_metadata.category.isNotNull()) &
    (product_metadata.title.isNotNull()) & (~product_metadata.title.rlike('^\\s*$')) &
    (~product_metadata.title.contains('getTime')) & (~product_metadata.title.contains('<span>'))
)

# replace in brand column occurrence of "Visit Amazon's <author> Page" to <author>
# otherwise use the brand column
product_metadata = product_metadata.withColumn(
    'temp_author',
    regexp_extract(product_metadata.brand, '^Visit Amazon\'s (.*) Page$', 1)
)

# create column has_amazon_author_page if the brand column contains "Visit Amazon's <author> Page"
product_metadata = product_metadata.withColumn(
    'has_amazon_author_page',
    product_metadata.brand.rlike('^Visit Amazon\'s .* Page$')
)

product_metadata = product_metadata.withColumn(
    "temp_author", when(product_metadata.temp_author == '', None).otherwise(product_metadata.temp_author)
)

# if author is empty, use brand
product_metadata = product_metadata.withColumn(
    'author',
    coalesce(product_metadata.temp_author, product_metadata.brand)
)

# extract the category name from the category column, taking the last item of the array
product_metadata = product_metadata.withColumn(
    'temp_category_1',
    element_at(product_metadata.category, 2)
)

product_metadata = product_metadata.withColumn(
    'temp_category_2',
    element_at(product_metadata.category, 1)
)

product_metadata = product_metadata.withColumn(
    'temp_category_3',
    element_at(product_metadata.category, 0)
)

# if fine_grained_category is empty, use main_cat
product_metadata = product_metadata.withColumn(
    'top_category',
    coalesce(product_metadata.temp_category_3, product_metadata.main_cat)
)

# if fine_grained_category is empty, use main_cat
product_metadata = product_metadata.withColumn(
    'sub_category',
    coalesce(product_metadata.temp_category_2, product_metadata.top_category)
)

# if fine_grained_category is empty, use main_cat
product_metadata = product_metadata.withColumn(
    'fine_grained_category',
    coalesce(product_metadata.temp_category_1, product_metadata.sub_category)
)

# drop columns that are not needed: category, main_cat, brand, also_buy, also_view, price, title
product_metadata = product_metadata.drop(
    'category', 'main_cat', 'brand', 'also_buy', 'also_view', 'price', 'title',
    'temp_author', 'temp_category_1', 'temp_category_2', 'temp_category_3'
)

product_metadata = product_metadata.withColumn('top_category', regexp_replace('top_category', '&amp;', '&')) \
    .withColumn('top_category', regexp_replace('top_category', '&lt;', '<')) \
    .withColumn('top_category', regexp_replace('top_category', '&gt;', '>')) \
    .withColumn('top_category', regexp_replace('top_category', '&quot;', '"')) \
    .withColumn('top_category', regexp_replace('top_category', '&apos;', "'")) \
    .withColumn('top_category', regexp_replace('top_category', '&nbsp;', ' ')) \
    .withColumn('top_category', regexp_replace('top_category', '<[^>]*>', '')) \
    .withColumn('sub_category', regexp_replace('sub_category', '&amp;', '&')) \
    .withColumn('sub_category', regexp_replace('sub_category', '&lt;', '<')) \
    .withColumn('sub_category', regexp_replace('sub_category', '&gt;', '>')) \
    .withColumn('sub_category', regexp_replace('sub_category', '&quot;', '"')) \
    .withColumn('sub_category', regexp_replace('sub_category', '&apos;', "'")) \
    .withColumn('sub_category', regexp_replace('sub_category', '&nbsp;', ' ')) \
    .withColumn('sub_category', regexp_replace('sub_category', '<[^>]*>', '')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&amp;', '&')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&lt;', '<')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&gt;', '>')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&quot;', '"')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&apos;', "'")) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '&nbsp;', ' ')) \
    .withColumn('fine_grained_category', regexp_replace('fine_grained_category', '<[^>]*>', ''))


# write the product metadata to hdfs
product_metadata.write.json('/user/s2236141/group22/books_cleaned', mode='overwrite', compression='gzip')
