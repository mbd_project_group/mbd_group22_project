# python2.7 project.py

from pyspark.sql import SparkSession

# create spark session
spark = SparkSession.builder.appName('repart_reviews').getOrCreate()

# set log level to ERROR
spark.sparkContext.setLogLevel('ERROR')

# read the amazon reviews file from the hdfs from path '/user/s2236141/group22/Books_5.json.gz'
# and create a dataframe from it, using the above schema
spark.read.json('/user/s2236141/group22/Books_5.json.gz')\
    .drop('image', 'reviewTime', 'summary', 'style')\
    .repartition(10)\
    .write.json('/user/s2236141/group22/reviews', mode='overwrite', compression='gzip')
